package com.example.ailaw;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.ml.modeldownloader.CustomModelDownloadConditions;
import com.google.firebase.ml.modeldownloader.DownloadType;
import com.google.firebase.ml.modeldownloader.FirebaseModelDownloader;

import org.tensorflow.lite.support.label.Category;
import org.tensorflow.lite.task.text.nlclassifier.BertNLClassifier;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PredictionActivity extends AppCompatActivity {
    private static final String TAG = "AILaw";

    private TextView resultTextView;
    private EditText inputEditText;
    private ExecutorService executorService;
    private ScrollView scrollView;
    private Button predictButton;

    // TODO 5: Define a BertNLClassifier variable
    private BertNLClassifier textClassifier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prediction);
        Log.v(TAG, "onCreate");

        executorService = Executors.newSingleThreadExecutor();
        resultTextView = findViewById(R.id.result_text_view);
        inputEditText = findViewById(R.id.input_text);
        scrollView = findViewById(R.id.scroll_view);

        predictButton = findViewById(R.id.predict_button);
        predictButton.setOnClickListener(
                (View v) -> {
                    classify(inputEditText.getText().toString());
                });

        // TODO 3: Call the method to download TFLite model
        downloadModel("ai-law");
    }

    /** Send input text to TextClassificationClient and get the classify messages. */
    private void classify(final String text) {
        executorService.execute(
                () -> {
                    // TODO 7: Run sentiment analysis on the input text
                    List<Category> results = textClassifier.classify(text);

                    // TODO 8: Convert the result to a human-readable text
                    String textToShow = "Kasus:\n" + text + "\n\nHasil:\n";
                    String Label, Result;
                    int index = 0;
                    int percentage = 0;
                    float Score = 0;
                    for (int i = 0; i < results.size(); i++) {
                        Category result = results.get(i);
                        if (Integer.parseInt(result.getLabel()) == 1) {
                            Label = "2 - 5 thn";
                        } else if(Integer.parseInt(result.getLabel()) == 2) {
                            Label = "5 - 10 thn";
                        } else if(Integer.parseInt(result.getLabel()) == 3) {
                            Label = "> 10 thn";
                        } else {
                            Label = "< 2 thn";
                        }
                        if (result.getScore() > Score) {
                            Score = Float.parseFloat(String.valueOf(result.getScore()));
                            index = Integer.parseInt(result.getLabel());
                        }
                        textToShow +=
                                String.format("    %s: %s\n", Label, result.getScore());
                    }
                    if (index == 1) {
                        Result = "2 - 5 thn";
                    } else if(index == 2) {
                        Result = "5 - 10 thn";
                    } else if(index == 3) {
                        Result = "> 10 thn";
                    } else {
                        Result = "< 2 thn";
                    }
                    percentage = Math.round(Score*100);
                    textToShow += "-------------------------------------\n";
                    textToShow += "Prediksi:\n";
                    textToShow += String.format("Kemungkinan hukuman pidana %s: %s%%\n", Result, percentage);

                    // Show classification result on screen
                    showResult(textToShow);
                });
    }

    /** Show classification result on the screen. */
    private void showResult(final String textToShow) {
        // Run on UI thread as we'll updating our app UI
        runOnUiThread(
                () -> {
                    // Append the result to the UI.
                    resultTextView.append(textToShow);

                    // Clear the input text.
                    inputEditText.getText().clear();

                    // Scroll to the bottom to show latest entry's classification result.
                    scrollView.post(() -> scrollView.fullScroll(View.FOCUS_DOWN));
                });
    }

    // TODO 2: Implement a method to download TFLite model from Firebase
    /** Download model from Firebase ML. */
    private synchronized void downloadModel(String modelName) {
        CustomModelDownloadConditions conditions = new CustomModelDownloadConditions.Builder()
                .requireWifi()
                .build();
        FirebaseModelDownloader.getInstance()
                .getModel("ai-law", DownloadType.LOCAL_MODEL, conditions)
                .addOnSuccessListener(model -> {
                    try {
                        // TODO 6: Initialize a TextClassifier with the downloaded model
                        textClassifier = BertNLClassifier.createFromFile(model.getFile());
                        predictButton.setEnabled(true);
                    } catch (IOException e) {
                        Log.e(TAG, "Failed to initialize the model. ", e);
                        Toast.makeText(
                                        PredictionActivity.this,
                                        "Model initialization failed.",
                                        Toast.LENGTH_LONG)
                                .show();
                        predictButton.setEnabled(false);
                    }
                })
                .addOnFailureListener(e -> {
                            Log.e(TAG, "Failed to download the model. ", e);
                            Toast.makeText(
                                            PredictionActivity.this,
                                            "Model download failed, please check your connection.",
                                            Toast.LENGTH_LONG)
                                    .show();

                        }
                );
    }
}